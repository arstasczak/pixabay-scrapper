//
//  Global.swift
//  PixabayScrapper
//
//  Created by Arkadiusz Staśczak on 14/06/2019.
//  Copyright © 2019 Arkadiusz Staśczak. All rights reserved.
//

import Foundation
import UIKit

let MAINCOLOR: UIColor = UIColor.init(red: 0/255, green: 77/255, blue: 153/255, alpha: 1)
let SECCOLOR: UIColor = UIColor.init(red: 230/255, green: 243/255, blue: 255/255, alpha: 1)


